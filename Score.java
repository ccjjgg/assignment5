/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignment5;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.text.*;

/**
 * 
 * @author Rikka
 */
public class Score extends JFrame implements ActionListener {
	/*
	 * GUI setting
	 */
	private JLabel lblscore[];
	private JTextField txtscore[], txtweight[];
	private JButton btncalculate;
	private JPanel pnlbutton, pnlscore;
	private final int size = 4;

	public Score() {
		pnlbutton = new JPanel();
		pnlscore = new JPanel();
		lblscore = new JLabel[2];
		lblscore[0] = new JLabel();
		lblscore[1] = new JLabel();
		lblscore[0].setBackground(Color.magenta);
		lblscore[0].setText("Testscore (0 - 100)");
		lblscore[0].setBackground(Color.red);
		lblscore[1].setText("Weight (0 - 1.00)");
		pnlscore.add(lblscore[0]);
		pnlscore.add(lblscore[1]);

		txtscore = new JTextField[size];
		txtweight = new JTextField[size];

		for (int i = 0; i < size; i++) {
			txtscore[i] = new JTextField("");
			txtscore[i].setBackground(Color.white);
			txtscore[i].setSize(12, 48);
			pnlscore.add(txtscore[i]);
			txtweight[i] = new JTextField("");
			txtweight[i].setBackground(Color.white);
			txtweight[i].setSize(12, 48);
			pnlscore.add(txtweight[i]);
		}

		btncalculate = new JButton("calculate");
		pnlscore.setBackground(Color.lightGray);
		pnlbutton.setBackground(Color.red);

		GridLayout gLayout = new GridLayout(5, 2);
		pnlscore.setLayout(gLayout);

		add(pnlscore, BorderLayout.CENTER);
		pnlbutton.add(btncalculate);
		add(pnlbutton, BorderLayout.SOUTH);
		btncalculate.addActionListener(this);
		setTitle("Score Weight Calculator");
		setVisible(true);
		setSize(320, 480);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
	}

	public void actionPerformed(ActionEvent e) {
		/*
		 * Action
		 */
		double finalscore = 0;
		double finalaverage = 0;
		for (int i = 0; i < size; i++) {
			String s1 = txtscore[i].getText();
			double s = Integer.valueOf(s1).intValue();
			String s2 = txtweight[i].getText();
			double w = Double.valueOf(s2).doubleValue();
			double tempscore = 0;
			tempscore = s * w;
			finalaverage += s / 4;
			finalscore += tempscore;
		}
		DecimalFormat df = new DecimalFormat("#.##");
		/*
		 * Print Out
		 */
		JOptionPane.showMessageDialog(null,
				"Finalscore: " + df.format(finalscore) + "\n"
						+ "Average score: " + df.format(finalaverage),
				"Final Score Weighted", JOptionPane.PLAIN_MESSAGE);
	}
}
